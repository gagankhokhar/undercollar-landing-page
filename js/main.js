$(window).load(function(){
     $('.preloader').fadeOut('slow');
});


/* =Main INIT Function
-------------------------------------------------------------- */
function initializeSite() {

	"use strict";

	//OUTLINE DIMENSION AND CENTER
	(function() {
	    function centerInit(){

			var sphereContent = $('.sphere'),
				sphereHeight = sphereContent.height(),
				parentHeight = $(window).height(),
				topMargin = (parentHeight - sphereHeight) / 2;

			sphereContent.css({
				"margin-top" : topMargin+"px"
			});

			var heroContent = $('.hero'),
				heroHeight = heroContent.height(),
				heroTopMargin = (parentHeight - heroHeight) / 2;

			heroContent.css({
				"margin-top" : heroTopMargin+"px"
			});

	    }

	    $(document).ready(centerInit);
		$(window).resize(centerInit);
	})();

	// Init effect 
	$('#scene').parallax();

};
/* END ------------------------------------------------------- */

/* =Document Ready Trigger
-------------------------------------------------------------- */
$(window).load(function(){

	initializeSite();
	(function() {
		setTimeout(function(){window.scrollTo(0,0);},0);
	})();

});
/* END ------------------------------------------------------- */


$('#countdown').countdown({
	date: "December 14, 2019 18:03:26",
	render: function(data) {
	  var el = $(this.el);
	  el.empty()
	    //.append("<div>" + this.leadingZeros(data.years, 4) + "<span>years</span></div>")
	    .append("<div>" + this.leadingZeros(data.days, 2) + " <span>days</span></div>")
	    .append("<div>" + this.leadingZeros(data.hours, 2) + " <span>hrs</span></div>")
	    .append("<div>" + this.leadingZeros(data.min, 2) + " <span>min</span></div>")
	    .append("<div>" + this.leadingZeros(data.sec, 2) + " <span>sec</span></div>");
	}
});


		jQuery(document).ready(function($) {
			// plugin

			// canvasAnimation1
			// Moveable Animation Start
			Matter.use('matter-wrap');
			const TEXTURES = "images/heart-outline.png";
			const TEXTURESdd = "https://cdn.shopify.com/s/files/1/0250/3642/3245/files/UnderCollar_Logo1.svg";

			let floatyBubbles = {
				// customizable options (passed into init function)
			    options: {
					canvasSelector: '',				// to find <canvas> in DOM to draw on
					radiusRange: [10, 30],			// random range of body radii
					xVarianceRange: [-0.5, 0.5],	// random range of x velocity scaling on bodies
					yVarianceRange: [0.5, 1.5],		// random range of y velocity scaling on bodies
					airFriction: 0.03,				// air friction of bodies
					opacity: 0.2,						// opacity of bodies
					collisions: false,				// do bodies collide or pass through
					scrollVelocity: 0.055,			// scaling of scroll delta to velocity applied to bodies
					pixelsPerBody: 180000,			// viewport pixels required for each body added

					// colors to cycle through to fill bodies
					colors: ['#49b0e5', '#e94991', '#e93f34', '#f6ea30'],
				},

				// throttling intervals (in ms)
				scrollDelay: 100,
				resizeDelay: 400,

				// throttling variables and timeouts
				lastOffset: undefined,
				scrollTimeout: undefined,
				resizeTimeout: undefined,

				// Matter.js objects
				engine: undefined,
				render: undefined,
				runner: undefined,
				bodies: undefined,

				// kicks things off
				init(options) {
					// override default options with incoming options

					var Engine = Matter.Engine,
					        Render = Matter.Render,
					        Runner = Matter.Runner,
					        Common = Matter.Common,
					        MouseConstraint = Matter.MouseConstraint,
					        Mouse = Matter.Mouse,
					        World = Matter.World,
					        Vertices = Matter.Vertices,
					        Svg = Matter.Svg,
					        Bodies = Matter.Bodies;

					this.options = Object.assign({}, this.options, options);

					let viewportWidth = document.documentElement.clientWidth;
					let viewportHeight = document.documentElement.clientHeight;

					this.lastOffset = window.pageYOffset;
					this.scrollTimeout = null;
					this.resizeTimeout = null;
				
					// engine
					this.engine = Matter.Engine.create();
					this.engine.world.gravity.y = 0;
				
					// render
					this.render = Matter.Render.create({
						canvas: document.querySelector(this.options.canvasSelector),
						engine: this.engine,
						options: {
							width: viewportWidth,
							height: viewportHeight,
							wireframes: false,
							background: 'transparent'
						}
					});
					Matter.Render.run(this.render);
				
					// runner
					this.runner = Matter.Runner.create();
					Matter.Runner.run(this.runner, this.engine);
				
					// bodies
					this.bodies = [];
					let totalBodies = Math.round(viewportWidth * viewportHeight / this.options.pixelsPerBody);
					for (let i = 0; i <= totalBodies; i++) {
						let body = this.createBody(viewportWidth, viewportHeight);
						this.bodies.push(body);
					}
					Matter.World.add(this.engine.world, this.bodies);

					// events
					window.addEventListener('scroll', this.onScrollThrottled.bind(this));
					window.addEventListener('resize', this.onResizeThrottled.bind(this));
				},
				
				// stop all the things
				shutdown() {
					Matter.Engine.clear(this.engine);
					Matter.Render.stop(this.render);
					Matter.Runner.stop(this.runner);

					window.removeEventListener('scroll', this.onScrollThrottled);
					window.removeEventListener('resize', this.onResizeThrottled);
				},
				
				// random number generator
				randomize(range) {
					let [min, max] = range;
					return Math.random() * (max - min) + min;
				},
				
				// create body with some random parameters
				createBody(viewportWidth, viewportHeight) {
					let x = this.randomize([0, viewportWidth]);
					let y = this.randomize([0, viewportHeight]);
					let radius = this.randomize(this.options.radiusRange);
					let color = this.options.colors[this.bodies.length % this.options.colors.length];
					let border = 'transparent';
					
					// var logoOne = Matter.Bodies.rectangle(400, -2800, 90, 140, {
			  //           density: 0.0005,
			  //           frictionAir: 0.01,
			  //           restitution: 0.3,
			  //           friction: 0.01,
			  //           render: {
			  //               sprite: {
			  //                   texture: TEXTURES[Math.floor(random(0, TEXTURES.length))],
			  //               }
			  //           }
			  //       });
					return Matter.Bodies.circle(x, y, radius, {

						render: {
							// fillStyle: color,
							// strokeStyle: "#FF0000",
							opacity: this.options.opacity,
							strokeStyle: color,
							fillStyle: 'transparent',
			 	          	lineWidth: 1,
							sprite: {
		                        texture: TEXTURES,
		                        fillStyle: color,
		                        // xScale: x*0.004,
                          		// yScale: y*0.007
		                        xScale: 0.4,
                                yScale: 0.4
		                    }

						},
						frictionAir: this.options.airFriction,
						collisionFilter: {
							group: this.options.collisions ? 1 : -1
						},
						plugin: {
							wrap: {
								min: { x: 0, y: 0 },
								max: { x: viewportWidth, y: viewportHeight }
							}
						}
					});
				},
				
				// enforces throttling of scroll handler
				onScrollThrottled() {
					if (!this.scrollTimeout) {
						this.scrollTimeout = setTimeout(this.onScroll.bind(this), this.scrollDelay);
					}
				},
				
				// applies velocity to bodies based on scrolling with some randomness
				onScroll() {
					this.scrollTimeout = null;

					let delta = (this.lastOffset - window.pageYOffset) * this.options.scrollVelocity;
					this.bodies.forEach((body) => {
						Matter.Body.setVelocity(body, {
							x: body.velocity.x + delta * this.randomize(this.options.xVarianceRange),
							y: body.velocity.y + delta * this.randomize(this.options.yVarianceRange)
						});
					});
				
					this.lastOffset = window.pageYOffset;
				},
				
				// enforces throttling of resize handler
				onResizeThrottled() {
					if (!this.resizeTimeout) {
						this.resizeTimeout = setTimeout(this.onResize.bind(this), this.resizeDelay);
					}
				},
				
				// restart everything with the new viewport size
				onResize() {
					this.shutdown();
					this.init();
				}
			};

			// wait for DOM to load
			window.addEventListener('DOMContentLoaded', () => {
				// start floaty bubbles background
				Object.create(floatyBubbles).init({
			        canvasSelector: '#canvasAnimation1'
			    });
			});

			// Moveable Animation End



			// canvasAnimation2
			// Static Animation Start
			const canvas = document.querySelector("#canvasAnimation2");
			canvas.width = window.innerWidth;
			canvas.height = window.innerHeight;

			const cntxt = canvas.getContext("2d");

			function Bubble(x, y, dy, radius) {
			  this.x = x;
			  this.y = y;
			  this.dy = dy;
			  this.radius = radius;

			  this.draw = function() {

			    cntxt.beginPath();
			    cntxt.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
			    cntxt.strokeStyle = "rgba(73, 176, 229, .4)";
			    cntxt.stroke();
			  };

			  this.update = function() {
			    if (this.y + this.radius < 0) {
			      this.y = window.innerHeight;
			      this.x = Math.random() * window.innerWidth;
			    }

			    this.y -= this.dy;

			    this.draw();
			  };
			}

			let bubArray = [];

			for (i = 0; i < 15; i++) {
			  let radius = Math.random() * (60 - 10) + 10;
			  let x = Math.random() * (window.innerWidth - radius * 2) + radius;
			  let y = Math.random() * window.innerHeight;
			  let dy = Math.random() * (6-0.5) + 0.5;
			  
			  
			  bubArray.push(new Bubble(x,y,dy,radius));
			}

			function animate() {
			  requestAnimationFrame(animate);
			  cntxt.clearRect(0, 0, innerWidth, innerHeight);
			  
			  for(i=0; i < bubArray.length; i ++) {
			    bubArray[i].update();
			  }
			}

			console.log(bubArray);

			animate();
			// Static Animation End

		});